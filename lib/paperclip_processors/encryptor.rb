require 'openssl'
module Paperclip
  class Encryptor < Processor

    def make
      i = attachment.instance
      key = Group.find(i.group_id).secret_key
      iv = i.iv



      data = file.read
      cipher = OpenSSL::Cipher::Cipher.new("AES-256-CBC")
      cipher.encrypt
      cipher.key = key
      cipher.iv = iv

      encrypted = cipher.update(data) + cipher.final

      f = Tempfile.new("encrypted", "/tmp")
      f << Base64.encode64(encrypted)
      f.flush
      file = f
      file
    end
  end
end
