class Upload < ActiveRecord::Base
  belongs_to :group
  has_attached_file :zoop,
    validate_media_type: false,
    preserve_files: false,
    processors: [:encryptor],
    styles: {original: {}}
  do_not_validate_attachment_file_type :zoop
end
