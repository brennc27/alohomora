class User < ActiveRecord::Base
  devise :database_authenticatable, :validatable, :authentication_keys => [:username]

  validates :public_key, presence: true

  has_many :owned_groups, foreign_key: "owner_id", class_name: "Group"
  has_many :group_users, dependent: :destroy
  has_many :groups, through: :group_users

  def email_required?
    false
  end

  def email_changed?
    false
  end
end
