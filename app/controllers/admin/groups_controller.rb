module Admin
  class GroupsController < Admin::ApplicationController
    before_action :filter_users, except: [:index, :new, :create]

    def filter_users
      group = Group.find(params["id"])
      unless current_user.id == group.owner_id
        flash[:error] = "You are not that group's owner!"
        redirect_to admin_groups_path
      end
    end
    # To customize the behavior of this controller,
    # simply overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = Group.all.paginate(10, params[:page])
    # end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   Group.find_by!(slug: param)
    # end

    # See https://administrate-docs.herokuapp.com/customizing_controller_actions
    # for more information
  end
end
