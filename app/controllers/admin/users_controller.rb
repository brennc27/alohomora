module Admin
  class UsersController < Admin::ApplicationController
    before_action :filter_users, except: [:index, :new, :create]

    def filter_users
      unless current_user.id == params["id"].to_i
        flash[:error] = "You are not that user!"
        redirect_to admin_users_path
      end
    end
    # To customize the behavior of this controller,
    # simply overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = User.all.paginate(10, params[:page])
    # end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   User.find_by!(slug: param)
    # end

    # See https://administrate-docs.herokuapp.com/customizing_controller_actions
    # for more information
  end
end
