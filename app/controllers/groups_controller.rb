class GroupsController < ApplicationController
  def index
    @groups = Group.all
  end

  def show
    @group = Group.includes(:uploads).find(params["id"])
    if current_user && @group.users.include?(current_user)
      @encoded_symetric_key = encode_key(current_user, @group.secret_key)
    end
  end

  def encode_key(user, secret_key)
    public_key = OpenSSL::PKey::RSA.new(user.public_key)
    Base64.encode64(public_key.public_encrypt(secret_key))
  end

  def add_file
    @group = Group.find(params["id"])
    filter_users
    @upload = Upload.new
  end

  def gen_iv
    SecureRandom.hex()
  end

  def create_file
    @group = Group.find(params["id"])

    filter_users

    upload = Upload.new(file_params)

    if upload.save!
      redirect_to group_path(@group.id)
    else
      redirect_to add_file_path(@group.id)
    end
  end

  private

  def file_params
    params["upload"]["iv"] = gen_iv
    params.require(:upload).permit(:iv, :group_id, :zoop)
  end

  def filter_users
    unless @group && @group.owner_id == current_user.id
      flash[:error] = "You are not that group's owner!"
      redirect_to admin_groups_path
    end
  end
end
