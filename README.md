# Cloud Encryption System - Conor Brennan 13327472

# Introduction

This assignment asked us to implement a key storage and encryption system for a
cloud file storage system. Files uploaded to the system should be encrypted and
only users in specific permissions and keys should  be able to decrypt the
files.

I decided to implement the file storage system instead of integrating with an
existing platform. I used Ruby and Rails to implement my solution. Ruby is a
dynamic, object-oriented language, and Rails is an MVC web framework for Ruby.

Some of the implementation is listed in this report, the entire application can
be found on gitlab:

`https://gitlab.scss.tcd.ie/brennc27/alohomora`

# Implementation

The implementation is split into two logical parts:

- A file storage system
- A key management and encryption system

The file storage system is of little interest for this assignment, I will give
only a brief explanation of its implementation.

I used the Paperclip package to handle file uploads and storage to the server.
The files are stored on disk, on the machine that the Rails process is running
on. Each file belongs to a Group, each Group has an owner and also has zero or
more authorized users. Only authorized users can upload to a group, but anyone
can view and download the files in a group, as they are encrypted before
storage.

Users sign in with a password and username, this login process is required to
keep the secret keys for file encryption secure. As no email is required by the
system, it is reasonably anonymous.

## Encryption

I gave a brief explanation of the data model above, and will now give further
detail. Each Group has a secret key - a random hex string generated when the
group is created. This key is used to encrypt all files uploaded to the group.
With this method, the server never stores the unencrypted file. When each file
is uploaded, an initialization vector is generated for it, again, this is used
in the file encryption process for further security.

This process is outlined in the following code snippet:

```ruby
key = Group.find(file.group_id).secret_key
iv = file.iv

data = file.read
cipher = OpenSSL::Cipher::Cipher.new("AES-256-CBC")
cipher.encrypt
cipher.key = key
cipher.iv = iv

encrypted = cipher.update(data) + cipher.final
```

All groups are publicly visible, as is a listing of the files in the group,
along with their initialization vectors.

Each user has a public key in the system. This is used whenever they want to
decrypt a file. As mentioned above, anyone can see the encrypted file contents
and the initialization vector, however, only users in a group can see the
secret key for the group. They can't see the plaintext secret key - when it is
displayed to a user it is first encrypted with their public key and then
encoded as a base64 string. This ensures that only the possessor of the entire
key pair can decode the secret key.

This encryption process is outlined by the following code snippet:

```ruby
def encode_key(user, secret_key)
  public_key = OpenSSL::PKey::RSA.new(user.public_key)
  Base64.encode64(public_key.public_encrypt(secret_key))
end
```

With the secret key (encrypted with the user's public key) and the file
contents (encrypted with the secret key), a user can decrypt a file by first
decrypting the secret key and then decyrpting the file.

The rendering of web pages and servicing of requests is a secondary concern for
this assignment, so I won't include code snippets. The 'web' portion of my
implementation is standard Rails code, following the MVC pattern.

I included a ruby script that can be added to you `$PATH` to decrypt the file.
It's contents are listed below:

```ruby
#!/Users/nub/.rbenv/shims/ruby

require 'openssl'
require 'base64'

if ARGV.length < 3
  puts 'usage: alohomora [symmetric_key] [file_specific_iv] [path_to_file]'
  exit
end

key = ARGV[0]
iv = ARGV[1]


encrypted = File.open(ARGV[2]).read
encrypted = Base64.decode64(encrypted)

decipher = OpenSSL::Cipher::Cipher.new("AES-256-CBC")
decipher.decrypt
decipher.key = key
decipher.iv = iv

plain = decipher.update(encrypted) + decipher.final
puts plain
```

With this script added to your path, files can be decryted:

```base
alohomora [symmetric_key] [file_initialization_vector] [path_to_file]
```

## Admin Section

New users can be added and new groups created through the management section of
the application. User authentication is handled using Devise - a comprehensive
authentication library for Ruby. The admin section consists of listings of the
users and groups and some forms to update them. Users can add other users, or
change their usernames and public keys as they wish.

As mentioned earlier, each group has an owner - decided by the user who created
the group. Only the owner of a group can add or remove other members from a
group.
