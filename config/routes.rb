Rails.application.routes.draw do
  root to: "groups#index"
  get "/" => "groups#index", as: :groups
  get "/groups/:id" => "groups#show", as: :group
  get "/groups/:id/add" => "groups#add_file", as: :add_file
  post "/groups/:id/add" => "groups#create_file", as: :create_file

  namespace :admin do
    resources :users
    resources :groups

    root to: "users#index"
  end

  devise_for :users
end
