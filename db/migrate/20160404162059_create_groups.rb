class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.references :owner, index: true, references: :users
      t.string :secret_key

      t.timestamps null: false
    end
  end
end
