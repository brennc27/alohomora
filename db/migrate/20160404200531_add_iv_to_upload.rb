class AddIvToUpload < ActiveRecord::Migration
  def change
    add_column :uploads, :iv, :string, null: false, default: ""
  end
end
