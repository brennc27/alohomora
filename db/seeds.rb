# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
fake_public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQCw0UoNzfYZuv0Smb3yTVbYw4Oy+MX2VaFPEmJbFxhtIUIWuQyy0ARa1wUu+ZNp1euiMsUie4mk2c7wymbvFstmK59exl+zK4biwLL4b3+k6N06ZRZDEkzKOb1iV3GEnG65/NvHfM82LYtl5ClUW6xcDoiGd7ySS6ncTlNoki55zQ== mk@mk3"
user = User.create!(username: "admin", password: "password", password_confirmation: "password", public_key: fake_public_key)
